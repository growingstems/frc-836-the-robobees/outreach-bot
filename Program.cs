using System.Threading;
using Microsoft.SPOT;
using System.Text;

using CTRE.Phoenix;
using CTRE.Phoenix.Controller;
using CTRE.Phoenix.MotorControl;
using CTRE.Phoenix.MotorControl.CAN;

namespace Hero_Arcade_Drive_Example
{
    public class Program
    {
        const bool IS_BENCHTOP = false;
        /**
         * Axes
         * 0 - Left X
         * 1 - Left Y
         * 2 - Right X
         * 5 - Right Y
         * 
         * Btns
         * 1 - X
         * 2 - A
         * 3 - B
         * 4 - Y
         * 5 - Left Bumper
         * 6 - Right Bumper
         * 7 - Left Trigger (Yes it's acting as a button)
         * 8 - Right Trigger
         * 9 - Back
         * 10 - Start
         * 11 - L3
         * 12 - R3
         * 
         * POV
         * 701
         * 682
         * 543
         */
        /* controls */
        const int forwardsAxis = 1; // Left Y
        const int turnAxis = 2; // Right X

        const int intakeButton = 6; // Right Bumper
        const int outtakeButton = 7; // Left Trigger
        const int retractButton = 5; // Left Bumper
        const double intakePower = 0.5;
        const double outtakePower = 0.5;
        const double deployPower = 0.3;
        const double serializerInPower = 0.5;
        const double serializerOutPower = 0.5;
        const double retractPower = 0.3;
        const double stayDeployedPower = 0.0;
        const double stayRetractedPower = 0.0;
        const int deployTimeMs = 500;
        const int retractTimeMs = 500;

        const int shootButton = 8; // Right Trigger
        const int shooterOffButton = 9; // Back
        const int shooterOnButton = 10; // Start
        const double shooterWheelPower = 0.7;
        const double shootFeedPower = 0.5;
        const int shootSpinupTimeMs = 2000;

        const double turretPower = 0.2;
        const double hoodPower = 0.1;
        const int hoodUpPov = 0;
        const int hoodDownPov = 4;
        const int turretLeftPov = 2;
        const int turretRightPov = 6;

        /* drive motors */
        static readonly TalonSRX leftPrimary = IS_BENCHTOP ? null : new TalonSRX(4);
        static readonly VictorSPX leftSecondary = IS_BENCHTOP ? null : new VictorSPX(6);
        static readonly TalonSRX rightPrimary = IS_BENCHTOP ? null : new TalonSRX(1);
        static readonly VictorSPX rightSecondary = IS_BENCHTOP ? null : new VictorSPX(7);
        /* intake motors */
        static readonly VictorSPX deploy = IS_BENCHTOP ? null : new VictorSPX(9);
        static readonly TalonSRX intake = IS_BENCHTOP ? null : new TalonSRX(5);
        /* tunnel motors */
        static readonly TalonSRX serializer = IS_BENCHTOP ? null : new TalonSRX(3);
        static readonly VictorSPX primer = IS_BENCHTOP ? null : new VictorSPX(10);
        /* shooter motors */
        static readonly TalonSRX hood = IS_BENCHTOP ? null : new TalonSRX(15);
        static readonly TalonSRX turret = IS_BENCHTOP ? null : new TalonSRX(2);
        static readonly VictorSPX shooterWheel = IS_BENCHTOP ? null : new VictorSPX(16);

        /* debug string builder */
        static readonly StringBuilder stringBuilder = new StringBuilder();
        static readonly GameController gamepad = new GameController(UsbHostDevice.GetInstance());

        /* state data */
        static bool isDeployed = false;
        static Stopwatch deployRetractStopwatch = null;
        static bool isShooterSpinning = false;
        static Stopwatch shooterStopwatch = null;
        static int frameCount = 0;

        public static void Main()
        {
            if (!IS_BENCHTOP)
            {
                /* Factory Default all hardware to prevent unexpected behaviour */
                leftPrimary.ConfigFactoryDefault();
                rightPrimary.ConfigFactoryDefault();
                leftSecondary.ConfigFactoryDefault();
                rightSecondary.ConfigFactoryDefault();
                deploy.ConfigFactoryDefault();
                intake.ConfigFactoryDefault();
                serializer.ConfigFactoryDefault();
                primer.ConfigFactoryDefault();
                hood.ConfigFactoryDefault();
                turret.ConfigFactoryDefault();
                shooterWheel.ConfigFactoryDefault();

                leftPrimary.SetInverted(true);
                rightPrimary.SetInverted(true);
                leftSecondary.SetInverted(true);
                rightSecondary.SetInverted(true);
                deploy.SetInverted(true);
                intake.SetInverted(false);
                serializer.SetInverted(true);
                primer.SetInverted(true);
                hood.SetInverted(false);
                turret.SetInverted(true);
                shooterWheel.SetInverted(false);

                leftSecondary.Follow(leftPrimary);
                rightSecondary.Follow(rightPrimary);

                turret.SetNeutralMode(NeutralMode.Brake);
                hood.SetNeutralMode(NeutralMode.Brake);
                shooterWheel.SetNeutralMode(NeutralMode.Coast);
            }

            // TODO: Configure primary/secondary drive motors to master/slave

            /* loop forever */
            while (true)
            {
                stringBuilder.Clear();
                printCount();
                // printControls();
                
                /* update subsystems */
                Drive();
                Intake();
                Tunnel();
                Shooter();
                
                /* print any existing debug info */
                if (stringBuilder.Length > 0)
                {
                    Debug.Print(stringBuilder.ToString());
                }
                stringBuilder.Clear();
                /* feed watchdog to keep Talon's enabled */
                Watchdog.Feed();
                /* run this task every 20ms */
                Thread.Sleep(20);
            }
        }

        static void printCount()
        {
            stringBuilder.Append("Count: ");
            stringBuilder.Append(frameCount++);
        }

        static void printControls()
        {
            GameControllerValues gamepadValues = new GameControllerValues();
            gamepad.GetAllValues(ref gamepadValues);
            stringBuilder.Append(" | Axes: ");
            foreach (var value in gamepadValues.axes)
            {
                stringBuilder.Append(value);
                stringBuilder.Append(", ");
            }
            stringBuilder.Append(" | Btns: ");
            stringBuilder.Append(gamepadValues.btns);
            stringBuilder.Append(" | Pov: ");
            stringBuilder.Append(gamepadValues.pov);
        }

        /**
         * If value is within 10% of center, clear it.
         * @param value [out] floating point value to deadband.
         */
        static void Deadband(ref float value)
        {
            if (value < -0.10)
            {
                /* outside of deadband */
            }
            else if (value > +0.10)
            {
                /* outside of deadband */
            }
            else
            {
                /* within 10% so zero it */
                value = 0;
            }
        }
        static void Drive()
        {
            float forwards = -1 * gamepad.GetAxis(forwardsAxis);
            float turn = -1 * gamepad.GetAxis(turnAxis);

            Deadband(ref forwards);
            Deadband(ref turn);

            float leftThrot = forwards - turn;
            float rightThrot = forwards + turn;

            if (!IS_BENCHTOP)
            {
                leftPrimary.Set(ControlMode.PercentOutput, leftThrot);
                rightPrimary.Set(ControlMode.PercentOutput, -rightThrot);
            }

            stringBuilder.Append(", f:");
            stringBuilder.Append(forwards);
            stringBuilder.Append(", turn:");
            stringBuilder.Append(turn);
            stringBuilder.Append(", l:");
            stringBuilder.Append(leftThrot);
            stringBuilder.Append(", r:");
            stringBuilder.Append(rightThrot);
        }
        static void Intake()
        {
            bool shouldIntake = gamepad.GetButton(intakeButton);
            bool shouldOuttake = gamepad.GetButton(outtakeButton);
            if (gamepad.GetButton(retractButton))
            {
                // RETRACTING
                if (!IS_BENCHTOP)
                {
                    intake.Set(ControlMode.Disabled, 0.0);
                }
                stringBuilder.Append(", i:0.0");
                if (isDeployed)
                {
                    stringBuilder.Append(", Begin Retracting");
                    isDeployed = false;
                    if (!IS_BENCHTOP)
                    {
                        deploy.Set(ControlMode.PercentOutput, -retractPower);
                    }
                    deployRetractStopwatch = new Stopwatch();
                    deployRetractStopwatch.Start();
                }
            }
            else if (shouldIntake || shouldOuttake)
            {
                // DEPLOYING/INTAKING/OUTTAKING
                double power = shouldIntake ? intakePower : -outtakePower;
                if (!IS_BENCHTOP)
                {
                    intake.Set(ControlMode.PercentOutput, power);
                }
                stringBuilder.Append(", i:");
                stringBuilder.Append(power);
                if (!isDeployed)
                {
                    stringBuilder.Append(", Begin Deploying");
                    isDeployed = true;
                    if (!IS_BENCHTOP)
                    {
                        deploy.Set(ControlMode.PercentOutput, deployPower);
                    }
                    deployRetractStopwatch = new Stopwatch();
                    deployRetractStopwatch.Start();
                }
            }
            else
            {
                // IDLE
                stringBuilder.Append(", i:0.0");
                if (!IS_BENCHTOP)
                {
                    intake.Set(ControlMode.Disabled, 0.0);
                }
            }
            // Handle deploy/retract timeouts
            if (deployRetractStopwatch != null)
            {
                if (isDeployed)
                {
                    if (deployRetractStopwatch.DurationMs > deployTimeMs)
                    {
                        stringBuilder.Append(", Finished Deploying");
                        if (!IS_BENCHTOP)
                        {
                            deploy.Set(ControlMode.PercentOutput, stayDeployedPower);
                        }
                        deployRetractStopwatch = null;
                    }
                }
                else
                {
                    if (deployRetractStopwatch.DurationMs > retractTimeMs)
                    {
                        stringBuilder.Append(", Finished Retracting");
                        if (!IS_BENCHTOP)
                        {
                            deploy.Set(ControlMode.PercentOutput, -stayRetractedPower);
                        }
                        deployRetractStopwatch = null;
                    }
                }
            }
        }
        static void Tunnel()
        {
            if (gamepad.GetButton(intakeButton) || gamepad.GetButton(shootButton))
            {
                stringBuilder.Append(", t1:");
                stringBuilder.Append(serializerInPower);
                if (!IS_BENCHTOP)
                {
                    serializer.Set(ControlMode.PercentOutput, serializerInPower);
                }
            }
            else if (gamepad.GetButton(outtakeButton))
            {
                stringBuilder.Append(", t1:");
                stringBuilder.Append(-serializerOutPower);
                if (!IS_BENCHTOP)
                {
                    serializer.Set(ControlMode.PercentOutput, -serializerOutPower);
                }
            }
            else
            {
                stringBuilder.Append(", t1:0.0");
                if (!IS_BENCHTOP)
                {
                    serializer.Set(ControlMode.Disabled, 0.0);
                }
            }
        }
        static void Shooter()
        {
            var shouldShoot = gamepad.GetButton(shootButton);
            var ensureStopped = gamepad.GetButton(shooterOffButton);
            var ensureSpinning = !ensureStopped && (shouldShoot || gamepad.GetButton(shooterOnButton));
            // Test for spinup
            if (ensureSpinning && !isShooterSpinning)
            {
                isShooterSpinning = true;
                shooterStopwatch = new Stopwatch();
                shooterStopwatch.Start();
                stringBuilder.Append(", SPIN-UP");
                if (!IS_BENCHTOP)
                {
                    shooterWheel.Set(ControlMode.PercentOutput, shooterWheelPower);
                }
            }
            // Test for spindown
            if (ensureStopped && isShooterSpinning)
            {
                isShooterSpinning = false;
                shooterStopwatch = null;
                stringBuilder.Append(", SPIN-DOWN");
                if (!IS_BENCHTOP)
                {
                    shooterWheel.Set(ControlMode.Disabled, 0.0);
                }
            }
            // Test for spinup finish
            if (shooterStopwatch != null && shooterStopwatch.DurationMs > shootSpinupTimeMs)
            {
                shooterStopwatch = null;
            }
            // Test for shoot
            if (shouldShoot && shooterStopwatch == null)
            {
                if (!IS_BENCHTOP)
                {
                    primer.Set(ControlMode.PercentOutput, shootFeedPower);
                }
                stringBuilder.Append(", t2:");
                stringBuilder.Append(shootFeedPower);
            }
            else
            {
                stringBuilder.Append(", t2:0.0");
                if (!IS_BENCHTOP)
                {
                    primer.Set(ControlMode.Disabled, 0.0);
                }
            }

            GameControllerValues values = new GameControllerValues();
            gamepad.GetAllValues(ref values);
            double hoodPowerNow = 0.0;
            double turretPowerNow = 0.0;
            stringBuilder.Append(", pov:");
            stringBuilder.Append(values.pov);
            switch (values.pov)
            {
                case hoodUpPov:
                    hoodPowerNow = hoodPower;
                    break;
                case hoodDownPov:
                    hoodPowerNow = -hoodPower;
                    break;
                case turretLeftPov:
                    turretPowerNow = turretPower;
                    break;
                case turretRightPov:
                    turretPowerNow = -turretPower;
                    break;
            }
            stringBuilder.Append(", h:");
            stringBuilder.Append(hoodPowerNow);
            stringBuilder.Append(", turr:");
            stringBuilder.Append(turretPowerNow);
            if (!IS_BENCHTOP)
            {
                hood.Set(ControlMode.PercentOutput, hoodPowerNow);
                turret.Set(ControlMode.PercentOutput, turretPowerNow);
            }
        }
    }
}
